set(plugin_src 
	
	init_cell_objects.cpp
	init_cell_lattice.cpp
	init_rectangle.cpp
	init_circle.cpp 
	tiff_reader.cpp
	init_hex_lattice.cpp
	
PARENT_SCOPE)
